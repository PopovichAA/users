/**
 * Режимы отображения
 */
const VIEW_MODES = {
	TABLE 		: 0,
	CARDS_BOX 	: 1,
	GROUPS_BOX  : 2,
};

/**
 * Режим отображения по умолчанию
 */
const DEFAULT_VIEW_MODE = VIEW_MODES.TABLE;

/**
 * Наименование группы для пользователей без группы
 */
const DEFAULT_GROUP = 'Others'

/**
 * Направления сортировки
 */
const SORT_ORDERS = {
	ASC  : 'asc',
	DESC : 'desc',
}

/**
 * Режимы сортировки
 */
const SORT_MODES = {
	NAME_ASC 	: `name_${SORT_ORDERS.ASC}`,
	NAME_DESC 	: `name_${SORT_ORDERS.DESC}`,
	JOB_ASC     : `job_${SORT_ORDERS.ASC}`,
	JOB_DESC 	: `job_${SORT_ORDERS.DESC}`,
	GROUP_ASC   : `group_${SORT_ORDERS.ASC}`,
	GROUP_DESC  : `group_${SORT_ORDERS.DESC}`,
};

/**
 * Режим сортировки по умолчанию
 */
const DEFAULT_SORT_MODE = SORT_MODES.NAME_ASC;

/**
 * Свойства пользователя, по которым осуществляется поиск
 */
const USER_SEARCH_PROPERTIES = [ 'name', 'job', 'group', 'phone', 'email' ];

export default { 
	VIEW_MODES,
	DEFAULT_VIEW_MODE,
	DEFAULT_GROUP,
	SORT_MODES,
	DEFAULT_SORT_MODE,
	USER_SEARCH_PROPERTIES,
	SORT_ORDERS 
};