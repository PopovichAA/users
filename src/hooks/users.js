import { useState, useEffect } from 'react';
import _ from 'lodash';
import UsersAPI from '../api/usersAPI';
import SessionStorageAPI from '../api/sessionStorageAPI';
import UC from '../constants/users';

/**
 * Загрузка пользователей
 * @param {string} sortMode - режим сортировки
 * @param {string} filter - фильтрующая строка
 * @returns {{ users: Array, isLoading : boolean }}
 */
function useLoad( sortMode, filter ) {
	const [loadedUsers, setLoadedUsers] = useState( [] );
	const [isLoading, setIsLoading] = useState( false );

	// загрузка пользователей
	useEffect( () => {
		setIsLoading( true );

		// если уже загружали, то берем из хранилища
		const usersInStorage = SessionStorageAPI.getItem( 'users' );
		if( usersInStorage ) {
			setLoadedUsers( JSON.parse( usersInStorage ) );
			setIsLoading( false ); 
		} else {
			UsersAPI.loadUsers()
		    	.then( ( users ) => {
		    		setLoadedUsers( users );

		    		// сохраняем результат в хранилище
		    		SessionStorageAPI.setItem( 'users', JSON.stringify( users ) );
		    	} )
			    .finally( () => {
			    	setIsLoading( false ); 
			    } );
		}	    	 
	}, [] );

	// сортируем загруженных пользователей
	const sortedUsers = useSort( loadedUsers, sortMode );

	// составляем хэш (Set) с идентификаторами отфильтрованных пользователей
	const filteredUsersId = useFilter( loadedUsers, filter );

	// отфильтрованный и отсортированный список пользователей
	const [users, setUsers] = useState( [] );

	// отфильтровываем отсортированных пользователей
	useEffect( () => {
		const sortedUsersInFilter = _.filter( sortedUsers,
			( user ) => filteredUsersId.has( user.id ) );
		setUsers( sortedUsersInFilter );
	}, [ sortedUsers, filteredUsersId ] );

	return { users, isLoading };
}

/**
 * Сортировка пользователей
 * @param {Array} users - загруженные пользователи 
 * @param {string} sortMode - режим сортировки
 * @returns {{ users : Array }}
 */
function useSort( users, sortMode ) {
	const [ sortedUsers, setSortedUsers ] = useState( [] );

	useEffect( () => {	
		const [ key, order ] = sortMode.split( '_' );
		setSortedUsers( _.orderBy( users, key, order ) );
	}, [ sortMode, users ] );

	return sortedUsers;
}

/**
 * Поиск пользователей по переданной строке.
 * Данный метод возвращает Set с идентификатороами подходящих пользователей
 * @param {Array} users - загруженные пользователи 
 * @param {string} filter - фильтрующая строка
 * @returns {{ users : Set }}
 */
function useFilter( users, filter ) {
	const [ filteredUsersId, setFilteredUsersId ] = useState( new Set() );

	useEffect( () => {
		const usersIdSet = new Set();

		if( filter ) {
			// фильтр в нижнем регистре
			const lowerFilter = filter.toLowerCase();

			// добавляем пользователей с подходящими свойствами
			_.forEach( users, ( user ) => {
				// бежим по свойствам пользователя для поиска
				for( let i = 0, toi = UC.USER_SEARCH_PROPERTIES.length; i < toi; i++ ) {
					const prop = UC.USER_SEARCH_PROPERTIES[ i ];

					// значение свойства пользователя в нижнем регистре
					const val = _.get( user, prop ) || '';
					const lowerVal = val.toLowerCase();
					if( lowerVal.includes( lowerFilter ) ) {
						usersIdSet.add( user.id );
						break;
					}
				}
			} );
		} else {
			// если фильтр не установлен, то добавляем всех
			_.forEach( users, ( user ) => {
				usersIdSet.add( user.id );
			} )
		}

		// запоминаем идентификаторы отфильтрованных пользователей
		setFilteredUsersId( usersIdSet );
	}, [ filter, users ] );

	return filteredUsersId;
}

/**
 * Группировка пользователей по группам
 * @param {Array} users - пользователи
 * @param {string} currentGroupId - идентификатор текущей группы
 * @param {string} sortMode - режим сортировки
 * @returns {Object}
 */
function useGrouping( users, currentGroupId, sortMode ) {
	// пользователи по группам
	const [ groupedUsers, setGroupedUsers ] = useState( {} );
	
	// группы
	const [ groups, setGroups ] = useState( [] );

	useEffect( () => {
		const usersByGroup = _.groupBy( users, ( user ) => user.group || UC.DEFAULT_GROUP );
		setGroupedUsers( usersByGroup );

		// превращаем наименования групп в объекты
		const groupNames = _.keys( usersByGroup );
		let fakeGroups = _.map( groupNames, 
			( groupName ) => ( { id : groupName, name : groupName } ) );

		// если сортировка не по группам	
		const groupSortOrder = ( sortMode === UC.SORT_MODES.GROUP_DESC ) ?
			UC.SORT_ORDERS.DESC : UC.SORT_ORDERS.ASC;
		fakeGroups = _.orderBy( fakeGroups, 'name', groupSortOrder );		

		setGroups( fakeGroups ); 
	}, [ users, sortMode ] );

	// пользователи текущей группы
	const [ currentGroupUsers, setCurrentGroupUsers ] = useState( [] );

	useEffect( () => {
		setCurrentGroupUsers( _.get( groupedUsers, currentGroupId, [] ) );
	}, [ groupedUsers, currentGroupId ] );

	return { groups, currentGroupUsers };
}

export default { useLoad, useGrouping };