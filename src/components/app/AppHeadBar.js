import React from 'react';
import { useLocation } from 'react-router-dom';
import { AppBar, Toolbar, Typography } from "@material-ui/core";
import AppHeadBarMenu from './AppHeadBarMenu';


/**
 * Заголовок приложения 
 */
export default function AppHeadBar() {
	const { pathname } = useLocation();
  const pageName = pathname === ( '/users' ) ? 'Users' : 'Home';

	return (
		<AppBar position="static">
      <Toolbar>
        <AppHeadBarMenu/>
        <Typography variant="h6">
          {pageName}
        </Typography>
      </Toolbar>
    </AppBar>
	);
}