import React, { useState, useCallback } from 'react';
import { Link } from 'react-router-dom';
import { IconButton, Menu, MenuItem } from "@material-ui/core";
import { Menu as MenuIcon } from '@material-ui/icons';


/**
 * Меню заголовка приложения
 */
export default function AppHeadBarMenu() {
  const [ anchorEl, setAnchorEl ] = useState( null );

  const clickHandler = useCallback( ( event ) => {
    setAnchorEl( event.currentTarget );
  }, [] );

  const closeHandler = useCallback( () => {
    setAnchorEl( null );
  }, [] );

  return (
    <>
      <IconButton edge="start" color="inherit" aria-label="menu" onClick={clickHandler}>
        <MenuIcon />
      </IconButton>
      <Menu
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={closeHandler}> 
          <MenuItem onClick={closeHandler} component={Link} to="/">Home</MenuItem>
          <MenuItem onClick={closeHandler} component={Link} to="/users">Users</MenuItem>
      </Menu>
    </>
  );
}