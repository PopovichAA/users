import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { CssBaseline } from "@material-ui/core";
import AppHeadBar from './AppHeadBar';
import Home from '../home';
import Users from '../users';


/**
 * Основной компонент приложения
 */
export default function App() {
  return (   
    <div>
      <CssBaseline/>
      <Router>
        <AppHeadBar/>        
        <Switch>        
          <Route path='/users' component={Users}/>        
          <Route path='/' component={Home}/>          
        </Switch>      
      </Router>
    </div>                 
  );
}