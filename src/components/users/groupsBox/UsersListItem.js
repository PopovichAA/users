import React from 'react';
import { ListItem, ListItemAvatar, ListItemText, Avatar } from '@material-ui/core';
import PropTypes from 'prop-types';


/**
 * Элемент списка пользователей
 * @param {Object} user - пользователь
 */
export default function UsersListItem( { user } ) {
	return (
		<ListItem button>
        	<ListItemAvatar>
	      		<Avatar src='/userAvatar.png'/>
          	</ListItemAvatar>
      		<ListItemText primary={user.name} secondary={user.job}/>        
        </ListItem>
	);        	
}

UsersListItem.propTypes = {
  user : PropTypes.object.isRequired,
};