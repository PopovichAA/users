import React from 'react';
import { List } from '@material-ui/core';
import PropTypes from 'prop-types';
import UsersListItem from './UsersListItem';


/**
 * Список пользователей
 * @param {Array} users - пользователи
 */
export default function UsersList( { users } ) {
	return (
		<List>
			{ users.map( ( user ) => <UsersListItem key={user.id} user={user}/> ) }
		</List>
	)
}

UsersList.propTypes = {
  users : PropTypes.array.isRequired,
};