import React, { useCallback } from 'react';
import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { Group as GroupIcon } from '@material-ui/icons';
import PropTypes from 'prop-types';


/**
 * Элемент списка групп пользователей
 * @param {Object} group - группа пользователей
 * @param {boolean} [isSelected] - признак выбранной группы
 * @param {Function} onGroupSelect - обработчик выбора группы
 */
export default function UsersGroupsListItem( { group, isSelected = false, onGroupSelect } ) {
	const selectHandler = useCallback( ( event ) => {
		onGroupSelect( group.id );
	}, [ onGroupSelect, group ] );
	
	return (
		<ListItem button selected={isSelected} onClick={selectHandler}>
        	<ListItemIcon>
        		<GroupIcon/>
          	</ListItemIcon>
      		<ListItemText primary={group.name} />        
        </ListItem>
	);        	
}

UsersGroupsListItem.propTypes = {
  group : PropTypes.object.isRequired,
  isSelected : PropTypes.bool,
  onGroupSelect : PropTypes.func.isRequired,
};