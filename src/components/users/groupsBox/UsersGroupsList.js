import React from 'react';
import { List } from '@material-ui/core';
import PropTypes from 'prop-types';
import UsersGroupsListItem from './UsersGroupsListItem';


/**
 * Список групп пользователей
 * @param {Array} groups - группы
 * @param {string} selectedGroupId - идентификатор выбранной группы
 * @param {Function} onGroupSelect - обработчик выбора группы
 */
export default function UsersGroupsList( { groups, selectedGroupId, onGroupSelect } ) {
	return (
		<List>
			{ groups.map( ( group ) => {
				const isSelected = selectedGroupId === group.id;
				return <UsersGroupsListItem key={group.id} group={group}
					isSelected={isSelected} onGroupSelect={onGroupSelect}/>
			} ) }
		</List>
	)
}

UsersGroupsList.propTypes = {
  groups : PropTypes.array.isRequired,
  selectedGroupId : PropTypes.string.isRequired,
  onGroupSelect : PropTypes.func.isRequired,
};