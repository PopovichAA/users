import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import PropTypes from 'prop-types';
import UsersGroupsList from './UsersGroupsList';
import UsersList from './UsersList';
import UsersHooks from '../../../hooks/users';


/**
 * Блок с группами пользователей 
 * @param {Array} users - пользователи
 * @param {string} selectedGroupId - идентификатор выбранной группы
 * @param {Function} onGroupSelect - обработчик выбора группы
 * @param {string} sortMode - режим сортировки
 */
export default function UsersGroupsBox( { users, selectedGroupId, onGroupSelect, sortMode } ) {
	const { groups, currentGroupUsers } = UsersHooks.useGrouping( users, selectedGroupId, sortMode );

	return (
		<Grid container spacing={2}>
			<Grid item xs={6}>
				<Typography component="h2" color='primary'> 
	      			GROUPS	      	
		        </Typography>
			</Grid>
			<Grid item xs={6}>
				<Typography component="h2" color='primary'> 
	      			USERS	      	
		        </Typography>
			</Grid>
			<Grid item xs={6}>
				<UsersGroupsList groups={groups} 
					selectedGroupId={selectedGroupId} onGroupSelect={onGroupSelect}/>
			</Grid>
			<Grid item xs={6}>
				<UsersList users={currentGroupUsers}/>
			</Grid>
		</Grid>
	)
}

UsersGroupsBox.propTypes = {
  users : PropTypes.array.isRequired,
  selectedGroupId : PropTypes.string.isRequired,
  onGroupSelect : PropTypes.func.isRequired,
  sortMode : PropTypes.string.isRequired,
};