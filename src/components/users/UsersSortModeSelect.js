import React, { useCallback } from 'react';
import { Select, MenuItem, makeStyles } from '@material-ui/core';
import { ArrowDownward as ArrowDownwardIcon, ArrowUpward as ArrowUpwardIcon } from '@material-ui/icons';
import PropTypes from 'prop-types';
import UC from '../../constants/users';

const useStyles = makeStyles({
  select : {
  	marginTop : 23
  }
})

/**
 * Выбор способа сортировки пользователей
 * @param {string} sortMode - способ сортировки
 * @param {Function} onChange - обработка смены способа сортировки
 */
export default function UsersSortModeSelect( { sortMode, onChange } ) {
	const classes = useStyles();
	const changeHandler = useCallback( ( event ) => {
		onChange( event.target.value );
	}, [ onChange ] );  
	return (
		<Select value={sortMode}  
			className={classes.select} onChange={changeHandler}>
			<MenuItem value={UC.SORT_MODES.NAME_ASC}>
				<ArrowDownwardIcon/>
				Name
			</MenuItem>
			<MenuItem value={UC.SORT_MODES.NAME_DESC}>
				<ArrowUpwardIcon/>
				Name
			</MenuItem>
			<MenuItem value={UC.SORT_MODES.JOB_ASC}>
				<ArrowDownwardIcon/>
				Job
			</MenuItem>
			<MenuItem value={UC.SORT_MODES.JOB_DESC}>
				<ArrowUpwardIcon/>
				Job
			</MenuItem>
			<MenuItem value={UC.SORT_MODES.GROUP_ASC}>
				<ArrowDownwardIcon/>
				Group
			</MenuItem>
			<MenuItem value={UC.SORT_MODES.GROUP_DESC}>
				<ArrowUpwardIcon/>
				Group
			</MenuItem>
		</Select>
	)
}

UsersSortModeSelect.propTypes = {
  sortMode : PropTypes.string.isRequired,
  onChange : PropTypes.func.isRequired,
};
