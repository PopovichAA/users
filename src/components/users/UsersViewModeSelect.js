import React, { useCallback } from 'react';
import { BottomNavigation, BottomNavigationAction } from '@material-ui/core';
import { List as ListIcon, RecentActors as RecentActorsIcon, Group as GroupIcon } from '@material-ui/icons';
import PropTypes from 'prop-types';
import UC from '../../constants/users';


/**
 * Выбор режима отображения пользователей
 * @param {number} viewMode - режим отображения
 * @param {Function} onChange - обработка смены режима отображения
 */
export default function UsersViewModeSelect( { viewMode, onChange } ) {
	const changeHandler = useCallback( ( event, newValue ) => {
		onChange( newValue );
	}, [ onChange ] );  
	return (
		<BottomNavigation value={viewMode} onChange={changeHandler}>
	    	<BottomNavigationAction value={UC.VIEW_MODES.TABLE} label="Table" icon={<ListIcon/>}/>
		    <BottomNavigationAction value={UC.VIEW_MODES.CARDS_BOX} label="Cards" icon={<RecentActorsIcon/>}/>
		    <BottomNavigationAction value={UC.VIEW_MODES.GROUPS_BOX} label="Groups" icon={<GroupIcon/>} />
		</BottomNavigation>
	)
}

UsersViewModeSelect.propTypes = {
  viewMode : PropTypes.number.isRequired,
  onChange : PropTypes.func.isRequired,
};
