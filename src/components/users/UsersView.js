import React, { useState } from 'react';
import PropTypes from 'prop-types';
import UsersTable from './table/UsersTable';
import UsersCardsBox from './cardsBox/UsersCardsBox';
import UsersGroupsBox from './groupsBox/UsersGroupsBox';
import UC from '../../constants/users';


/**
 * Отображение пользователя от режима
 * @param {Array} users - пользователи
 * @param {number} viewMode - режим отображения
 * @param {string} sortMode - режим сортировки
 */
export default function UsersView( { users, viewMode, sortMode } ) {
	// для восстановления работы третьей вкладки
	const [ selectedGroupId, setSelectedGroupId ] = useState( '' );

	switch( viewMode ) {
		case UC.VIEW_MODES.CARDS_BOX:
			return <UsersCardsBox users={users}/>;

		case UC.VIEW_MODES.GROUPS_BOX:		
			return <UsersGroupsBox users={users} sortMode={sortMode}
				selectedGroupId={selectedGroupId} onGroupSelect={setSelectedGroupId}/>;			
		
		case UC.VIEW_MODES.TABLE:
		default:
			return <UsersTable users={users}/>;
	}
}

UsersView.propTypes = {
  users : PropTypes.array.isRequired,
  viewMode : PropTypes.number.isRequired,
  sortMode : PropTypes.string.isRequired,
};
