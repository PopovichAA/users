import React, { useState } from 'react';
import { Container, Grid, makeStyles } from '@material-ui/core';
import SearchInput from '../common/SearchInput';
import Loader from '../common/Loader';
import UsersView from './UsersView';
import UsersViewModeSelect from './UsersViewModeSelect';
import UsersSortModeSelect from './UsersSortModeSelect';
import UsersHooks from '../../hooks/users';
import UC from '../../constants/users';


const useStyles = makeStyles( (theme) => ( {
	container : {
		flexGrow : 1,
		height : '65vh',
		overflow: 'auto',
	},
	loader : {
		marginLeft : '40vw',
	    marginTop : '15vh',
	}
} ) );

/**
 * Страница с пользователями
 */
export default function Users() {
	const classes = useStyles();

	const [ filter, setFilter ] = useState( '' );
	const [ sortMode, setSortMode ] = useState( UC.DEFAULT_SORT_MODE );
	const [ viewMode, setViewMode ] = useState( UC.DEFAULT_VIEW_MODE );

	const { isLoading, users } = UsersHooks.useLoad( sortMode, filter );
	
	return (		
		<Container>
			<Grid container spacing={2}>
				<Grid item container xs={12} spacing={1}>
					<Grid item xs={11}>
						<SearchInput onChange={setFilter} searchText={filter}/>
					</Grid>
					<Grid item xs={1}>
						<UsersSortModeSelect onChange={setSortMode} sortMode={sortMode}/>
					</Grid>
				</Grid>		 
				<Grid item xs={12} className={classes.container}>
					{
						isLoading ? 
							<Loader size={150} className={classes.loader}/> : 
							<UsersView users={users} viewMode={viewMode} sortMode={sortMode}/>
					}	
				</Grid>     							
				<Grid item xs={12}>
					<UsersViewModeSelect viewMode={viewMode} onChange={setViewMode}/>	
				</Grid>	
			</Grid>
		</Container>			
	);
}