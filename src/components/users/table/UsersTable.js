import React from 'react';
import { TableContainer, Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import PropTypes from 'prop-types';
import UsersTableRow from './UsersTableRow';


/**
 * Таблица с пользователями
 * @param {Array} users - пользователи
 */
export default function UsersTable( { users } ) {
	return (
		<TableContainer>
	      <Table stickyHeader aria-label="Users table" >
	        <TableHead>
	          <TableRow>
	            <TableCell align="center">Name</TableCell>
	            <TableCell align="center">Job</TableCell>
	            <TableCell align="center">Group</TableCell>
	            <TableCell align="center">Email</TableCell>
	            <TableCell align="center">Phone</TableCell>
	          </TableRow>
	        </TableHead>
	        <TableBody>
	         	{ users.map( ( user ) => (
	         		<UsersTableRow user={user} key={user.id}/> 
         		) ) }         			
	        </TableBody>
	      </Table>
	    </TableContainer>
	);
}

UsersTable.propTypes = {
  users : PropTypes.array.isRequired,
};
