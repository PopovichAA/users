import React from 'react';
import { TableRow, TableCell } from '@material-ui/core';
import PropTypes from 'prop-types';


/**
 * Строка таблицы с пользователями
 * {Object} user - пользователь
 */
export default function UsersTableRow( { user } ) {
	return (
		<TableRow>
	    	<TableCell align="center">{user.name}</TableCell>
	        <TableCell align="center">{user.job}</TableCell>
	        <TableCell align="center">{user.group || '-'}</TableCell>
	        <TableCell align="center">{user.email}</TableCell>
	        <TableCell align="center">{user.phone}</TableCell>
    	</TableRow>
	);        	
}

UsersTableRow.propTypes = {
  user : PropTypes.object.isRequired,
};
