import React from 'react';
import { Grid, Card, CardContent, Typography, makeStyles, Avatar } from '@material-ui/core';
import PropTypes from 'prop-types';


const useStyles = makeStyles(( theme ) => ({
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
   large: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
} ) );

/**
 * Карточка пользователя
 * @param {Object} user - пользователь
 */
export default function UserCard ( { user } ) {
	const classes = useStyles();
	return (
		<Card>
	      <CardContent>
		      <Grid container spacing={2}>
		      	<Grid item xs={10}>
					<Typography component="h2">
			      		{user.name}	      	
			        </Typography>
			        <Typography className={classes.title} color="textSecondary" gutterBottom>
			        	{user.job}
			        </Typography>	        
			        <Typography className={classes.pos} color="textSecondary">
			        	{user.group || '-'}
			        </Typography>
			        <Typography variant="body2" component="p">
			        	{user.phone}  
			        </Typography>
			        <Typography variant="body2" component="p">
			        	{user.email}  
			        </Typography>
		      	</Grid>
		      	<Grid item xs={2}>
		      		<Avatar src='/userAvatar.png' className={classes.large}/>
		      	</Grid>
		      </Grid>	   
	      </CardContent>
	    </Card>
	);
}

UserCard.propTypes = {
  user : PropTypes.object.isRequired,
};