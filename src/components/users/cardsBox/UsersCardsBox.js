import React from 'react';
import { GridList, GridListTile } from '@material-ui/core';
import PropTypes from 'prop-types';
import UserCard from './UserCard';


/**
 * Блок с карточками пользователей
 * @param {Array} users - пользователи
 */
export default function UsersCardsBox ( { users } ) {
	return (
		<GridList cols={3}>   
			{ users.map( ( user ) => (
				<GridListTile key={user.id}>
					<UserCard user={user}/>
				</GridListTile>
     		) ) }        
    	</GridList>
	)	
}

UsersCardsBox.propTypes = {
  users : PropTypes.array.isRequired,
};