import React from 'react';
import { CircularProgress } from '@material-ui/core';
import PropTypes from 'prop-types';


/**
 * Индикатор загрузки
 * @param {number} [size] - размер 
 * @param {string} [className] - стили
 */
export default function Loader( { size, className } ) {	
	return (
		<CircularProgress className={className} size={size}/>		
	);
}

Loader.propTypes = {
  size: PropTypes.number,
  className : PropTypes.string,
};