import React, { useCallback } from 'react';
import { TextField } from '@material-ui/core';
import _ from 'lodash';
import PropTypes from 'prop-types';

/**
 * Строка поиска
 * @param {Function} onChange - обработчик изменения
 * @param {String} [searchText] - текст поиска
 */
export default function SearchInput( { onChange, searchText = '' } ) {	
	// устанавливаем задержку, для избежания частого срабатывания
	const debouncedChangeHandler = useCallback( _.debounce( ( val ) => {		
		onChange( val );
	}, 250 ), [ onChange ] );

	// добавляем второй колбэк из-за специфики обработки событий реакта
	const searchHandler = useCallback( ( event ) => {
		debouncedChangeHandler( event.target.value );
	}, [ debouncedChangeHandler ] );

	return (
		<TextField fullWidth  margin="normal" label='Search...' 
			defaultValue={searchText} onChange={searchHandler}/>
	);
}

SearchInput.propTypes = {
  onChange: PropTypes.func.isRequired,
  searchText : PropTypes.string,
};