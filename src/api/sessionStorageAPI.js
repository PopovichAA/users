import sessionstorage from 'sessionstorage';


/**
 * Сохранение значения по ключу
 * @param {string} key - ключ
 * @param {string} value - значение
 */
function setItem( key, value ) {
	sessionstorage.setItem( key, value );
}

/**
 * Получение значения по ключу
 * @param {string} key - ключ
 * @returns {string} 
 */
function getItem( key ) {
	return sessionstorage.getItem( key );
}

/**
 * Удаление значения
 * @param {string} key - ключ
 */
function removeItem( key ) {
	sessionstorage.removeItem( key );
}

/**
 * Очистка хранилища
 */
function clear() {
	sessionstorage.clear();
}

export default { setItem, getItem, removeItem, clear };