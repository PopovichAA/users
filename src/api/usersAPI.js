import axios from 'axios';


/**
 * Загрузка пользователей
 * @returns {Promise}
 */
function loadUsers() {
	return axios( 'data/users.json' )
		.then( ( resp ) => {
			return new Promise( ( resolve ) => {
				// имитируем долгую загрузку данных
				setTimeout( () => {
					resolve( resp.data );
				}, 1000 )
			} );		
		} )
		.catch( ( err ) => {
			console.error( `Something went wrong ${ err }` );
			return []; 
		} ); 
}

export default { loadUsers };